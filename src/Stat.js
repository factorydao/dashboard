import './App.css';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import stats from './stats.json';
import { Statistic } from 'antd';
import Token from "./contracts/Token.json";
import { useQuery } from "react-query";
import { ethInstance, fromWei } from "evm-chain-scripts";

console.log(stats)

async function getTokenBalance(network, tokenAddress, target) {
  console.log('getTokenBalance', )
  let balance
  if (tokenAddress === '') {
    balance = await ethInstance.readWeb3(network)?.eth.getBalance(target) || '0';
  } else {
    const token = await ethInstance.getReadContractByAddress(Token, tokenAddress, network);
    balance = await token.methods.balanceOf(target).call();
  }
  return fromWei(balance);
}

const useTokenBalance = (networkId, tokenAddress, target) => useQuery([networkId, tokenAddress, target], async () => await getTokenBalance(networkId, tokenAddress, target));

function Stat({networkId, targetAddress, tokenAddress, tokenName }) {
  const {data: balance} = useTokenBalance(networkId, tokenAddress, targetAddress)

  return (
    <Statistic title={tokenName} value={balance} precision={2} />
  )
}

export default Stat;
